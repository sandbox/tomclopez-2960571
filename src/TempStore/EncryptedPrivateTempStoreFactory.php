<?php

namespace Drupal\encrypted_tempstore\TempStore;

use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Creates a EncryptedPrivateTempStore object for a given collection.
 */
class EncryptedPrivateTempStoreFactory extends PrivateTempStoreFactory {

  /**
   * Creates an EncryptedPrivateTempStore.
   *
   * @param string $collection
   *   The collection name to use for this key/value store. This is typically
   *   a shared namespace or module name, e.g. 'views', 'entity', etc.
   *
   * @return \Drupal\encrypted_tempstore\TempStore\EncryptedPrivateTempStore
   *   An instance of the encrypted key/value store.
   */
  public function get($collection) {
    // Store the data for this collection in the database.
    $storage = $this->storageFactory->get("tempstore.private.$collection");
    return new EncryptedPrivateTempStore($storage, $this->lockBackend, $this->currentUser, $this->requestStack, $this->expire);
  }
}