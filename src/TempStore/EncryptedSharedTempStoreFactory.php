<?php

namespace Drupal\encrypted_tempstore\TempStore;

use Drupal\Core\TempStore\SharedTempStoreFactory;

/**
 * Creates a EncryptedSharedTempStore object for a given collection.
 */
class EncryptedSharedTempStoreFactory extends SharedTempStoreFactory {

  /**
   * Creates an EncryptedSharedTempStore for the current user or anonymous session.
   *
   * @param string $collection
   *   The collection name to use for this key/value store. This is typically
   *   a shared namespace or module name, e.g. 'views', 'entity', etc.
   * @param mixed $owner
   *   (optional) The owner of this SharedTempStore. By default, the
   *   SharedTempStore is owned by the currently authenticated user, or by the
   *   active anonymous session if no user is logged in.
   *
   * @return \Drupal\encrypted_tempstore\TempStore\EncryptedSharedTempStore
   *   An instance of the key/value store.
   */
  public function get($collection, $owner = NULL) {
    // Use the currently authenticated user ID or the active user ID unless
    // the owner is overridden.
    if (!isset($owner)) {
      $owner = \Drupal::currentUser()->id() ?: session_id();
    }

    // Store the data for this collection in the database.
    $storage = $this->storageFactory->get("tempstore.shared.$collection");
    return new EncryptedSharedTempStore($storage, $this->lockBackend, $owner, $this->requestStack, $this->expire);
  }
}