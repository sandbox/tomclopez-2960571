<?php

namespace Drupal\encrypted_tempstore;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

/**
* Overrides the core services tempstore.private and tempstore.shared.
*/
class EncryptedTempstoreServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {
  public function alter(ContainerBuilder $container) {
    $privateDefinition = $container->getDefinition('tempstore.private');
    $privateDefinition->setClass(
      'Drupal\encrypted_tempstore\TempStore\EncryptedPrivateTempStoreFactory'
    );

    $sharedDefinition = $container->getDefinition('tempstore.shared');
    $sharedDefinition->setClass(
      'Drupal\encrypted_tempstore\TempStore\EncryptedSharedTempStoreFactory'
    );
  }
}